![Project T.A.R.D.I.S.](https://gitlab.com/matthiasrigaud/Project-T.A.R.D.I.S./raw/master/assets/Cover.png)

## Instructions

Use Python3 and Pygame to work, so Linux users must install them (use your package manager for python, i. e. `pacman -S python3`, and pip to install pygame `sudo pip install pygame`).

A [stand-alone executable for Windows](https://matoux42.itch.io/project-tardis) is available.

## Story

You're the team leader in the **Test And Relative Driving In Simulation** project, shortened in Project T.A.R.D.I.S. This project aims to make the greatest space warship in the world, but it's not easy ! You'll have to recruit scientists and make some research and tests to reach this goal. Good luck !

## How to play

You'll have two parts : in the first part, you'll have to use wisely your money to manage your laboratories and upgrade your ship. In the second part, you'll have to test your ship in a simulator. Each monster killed give you money.

## Controls

### "Management" part

* Use  your cursor to interact with your environment

### "Simulation" part

* left/right arrows keys to move
* space to shoot

## LD41

This game was made for the LD41 Compo : [LD41 Profile](https://ldjam.com/events/ludum-dare/41/project-t-a-r-d-i-s).