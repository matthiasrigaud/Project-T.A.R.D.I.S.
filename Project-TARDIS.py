#!/usr/bin/env python3

##
## Main file
##

import pygame
import sys

sys.path.append('.')

from src import Constant, Manager_Interface, Event, Ship, Enemy_skeleton, Simulation

if __name__ == '__main__':

    pygame.init()
    screen = pygame.display.set_mode((Constant.WIN_WIDTH, Constant.WIN_HEIGHT))
    pygame.display.set_caption(Constant.WIN_TITLE)
    screen.fill([255, 255, 255])
    ship = Ship.Ship()
    enemy_skeleton = Enemy_skeleton.Enemy_skeleton()
    manager_interface = Manager_Interface.Manager_Interface(0, 5, ship, enemy_skeleton)
    old_time_manager = 0
    old_time_simulation = 0
    scop = "MANAGER"
    simulation = Simulation.Simulation(enemy_skeleton, ship)
    money = 300
    remaining_time_simulation = 0

    while 1:

        cur_time = pygame.time.get_ticks()
        elapsed_time_simulation = cur_time - old_time_simulation
        old_time_simulation = cur_time

        Event.manager(ship, scop)
        if scop == "MANAGER":
            elapsed_time_manager = cur_time - old_time_manager
            old_time_manager = cur_time
            scop, money = manager_interface.update(elapsed_time_manager, money)
            manager_interface.draw(screen)
            remaining_time_simulation = 60000
        elif scop == "SIMULATION":
            scop, money = simulation.update(elapsed_time_simulation, money, remaining_time_simulation)
            remaining_time_simulation = -1
            simulation.draw(screen)
        pygame.display.flip()

        pygame.time.wait(Constant.FRAME_TIME - (pygame.time.get_ticks() - cur_time))
