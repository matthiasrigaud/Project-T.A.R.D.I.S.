##
## Ship Management
##

import pygame

from . import Constant, User_interface, Bullet

class Ship:

    def __init__(self):
        self.skin = pygame.image.load(Constant.ASSETS_DIR + "ship.png")
        self.max_life = 50
        self.life = self.max_life
        self.damage = 5
        self.speed = 0.5
        self.position = [(Constant.WIN_WIDTH - Constant.INTRF_SIDE_BAR_WIDTH - self.skin.get_size()[0]) / 2, Constant.WIN_HEIGHT - 10 - self.skin.get_size()[1]]
        self.movement = 0
        self.is_shooting = False
        self.cooldown = 0
        self.shoot_bar = User_interface.Progress_Bar([Constant.INTRF_SIDE_BAR_X + 10, 650], [280, 20], 2, [[255, 255, 255], [220, 220, 220], [255, 0, 0]], 100)
        self.bullets = []

    def get_life_percent(self):
        return (self.life / self.max_life) * 100

    def increase_life(self, multiplicator):
        old = self.max_life
        self.max_life *= multiplicator
        self.life += self.max_life - old

    def get_max_life(self):
        return self.max_life

    def increase_damage(self, multiplicator):
        self.damage *= multiplicator

    def increase_speed(self, multiplicator):
        self.speed /= multiplicator

    def get_position(self):
        return self.position

    def repair(self, units):
        self.life += units
        if self.life > self.max_life:
            self.life = self.max_life

    def get_missing_life(self):
        return self.max_life - self.life

    def take_damages(self, damages):
        self.life -= damages

    def get_damage(self):
        return self.damage

    def get_speed(self):
        return 1 / self.speed

    def move_right(self, is_moving):
        if is_moving:
            self.movement += 5
        else:
            self.movement -= 5

    def move_left(self, is_moving):
        if is_moving:
            self.movement -= 5
        else:
            self.movement += 5

    def hard_movement_reset(self):
        self.movement = 0

    def shoot(self, is_shooting):
        self.is_shooting = is_shooting

    def collision(self, enemy):
        if pygame.Rect(self.position, self.skin.get_size()).colliderect(enemy.get_dimention()):
            self.life -= enemy.get_damage()
            return True
        return False

    def update(self, time_elapsed, enemies, money):
        for i in iter(self.bullets):
            if not i.update():
                self.bullets.remove(i)
                continue
            for j in iter(enemies):
                if j.get_dimention().colliderect(i.get_dimention()):
                    if j.get_degat(self.damage):
                        money += j.get_gain()
                        enemies.remove(j)
                    self.bullets.remove(i)
        self.position[0] += self.movement
        if self.position[0] < 0:
            self.position[0] = 0
        elif self.position[0] + self.skin.get_size()[0] > Constant.WIN_WIDTH - Constant.INTRF_SIDE_BAR_WIDTH:
            self.position[0] = Constant.WIN_WIDTH - Constant.INTRF_SIDE_BAR_WIDTH - self.skin.get_size()[0]
        self.cooldown -= time_elapsed
        self.shoot_bar.update(100 - (self.cooldown / (self.speed * 1000)) * 100, True)
        if self.is_shooting and self.cooldown <= 0:
            self.bullets.append(Bullet.MyBullet([self.position[0] + self.skin.get_size()[0] / 2, self.position[1]], self.damage))
            self.cooldown = self.speed * 1000
        return money

    def draw(self, screen, pos = None):
        if not pos:
            screen.blit(self.skin, self.position)
            self.shoot_bar.draw(screen)
            for i in iter(self.bullets):
                i.draw(screen)
        else:
            screen.blit(self.skin, pos)
