##
## define enemy
##

import pygame

from random import randint

from . import Enemy_skeleton, Constant

class Enemy:

    def __init__(self, skeleton):
        self.armor = skeleton.get_armor()
        self.speed = skeleton.get_speed()
        self.damage = skeleton.get_damage()
        self.gain = skeleton.get_gain()
        self.skin = pygame.image.load(Constant.ASSETS_DIR + "enemy.png")
        self.skin = pygame.transform.scale(self.skin, [self.skin.get_size()[0] * 2, self.skin.get_size()[1] * 2])
        self.pos = [randint(0, Constant.WIN_WIDTH - Constant.INTRF_SIDE_BAR_WIDTH - self.skin.get_size()[0]), 0]
        self.speed = [0, randint(2, 5)]

    def get_dimention(self):
        return pygame.Rect(self.pos, self.skin.get_size())

    def get_damage(self):
        return self.damage

    def get_degat(self, degat):
        self.armor -= degat
        if self.armor <= 0:
            return True
        return False

    def get_gain(self):
        return self.gain

    def update(self):
        self.pos[0] += self.speed[0]
        self.pos[1] += self.speed[1]
        if self.pos[1] > Constant.WIN_HEIGHT:
            return False
        return True

    def draw(self, screen):
        screen.blit(self.skin, self.pos)
