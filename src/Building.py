##
## Buildings
##

import pygame

from . import Constant, User_interface

class Enemy_Action:

    def __init__(self, skeleton):
        self.state = Constant.LAB_W8_4_RSRCH
        self.skeleton = skeleton
        self.price = 60
        self.wait_time = 12000
        self.nb_scientists = 4
        self.augmentation = 1.35
        self.enemy_multiplicator = 1.5
        self.font = pygame.font.Font(Constant.FONT_PATH, 15)
        self.search_button = User_interface.Button(Constant.FONT_PATH, [250, 25],
                                                   [700, Constant.INTRF_BOTTOM_BAR_Y + 60], 2,
                                                   Constant.UNBUYABLE_BUTTON, {
                                                       "TXT": "Launch Research",
                                                       "SIZE": 15,
                                                       "COLOR": [[200, 200, 200], [250, 250, 250], [250, 250, 250]]})
        self.buy_button = User_interface.Button(Constant.FONT_PATH, [250, 25],
                                                [700, Constant.INTRF_BOTTOM_BAR_Y + 60], 2,
                                                Constant.UNBUYABLE_BUTTON, {
                                                    "TXT": "Buy it !",
                                                    "SIZE": 15,
                                                    "COLOR": [[200, 200, 200], [250, 250, 250], [250, 250, 250]]})
        self.search_bar = User_interface.Progress_Bar([700, Constant.INTRF_BOTTOM_BAR_Y + 60], [200, 15], 2, [[255, 255, 255], [200, 200, 200], [0, 0, 255]], 100)

    def update(self, money, scientists, scientists_limit, free_scientists, elapsed_time, state):
        if not state and self.state == Constant.LAB_W8_4_RSRCH:
            if int(self.nb_scientists) > free_scientists:
                self.search_button.set_color(Constant.UNBUYABLE_BUTTON)
            else:
                self.search_button.set_color(Constant.BUYABLE_BUTTON)
                if self.search_button.get_state() != 0 or not pygame.mouse.get_pressed()[0]:
                    if self.search_button.is_overflew():
                        if pygame.mouse.get_pressed()[0]:
                            free_scientists -= int(self.nb_scientists)
                            self.search_button.set_state(0)
                            self.state = Constant.LAB_IN_RSRCH
                        else:
                            self.search_button.set_state(1)
                    else:
                        self.search_button.set_state(2)
        elif not state and self.state == Constant.LAB_W8_4_BUY:
            if self.price > money:
                self.buy_button.set_color(Constant.UNBUYABLE_BUTTON)
            else:
                self.buy_button.set_color(Constant.BUYABLE_BUTTON)
                if self.buy_button.get_state() != 0 or not pygame.mouse.get_pressed()[0]:
                    if self.buy_button.is_overflew():
                        if pygame.mouse.get_pressed()[0]:
                            self.skeleton.increase_stats(self.enemy_multiplicator)
                            self.buy_button.set_state(0)
                            money -= self.price
                            self.price *= self.augmentation
                            self.wait_time *= self.augmentation
                            self.nb_scientists *= self.augmentation
                            self.augmentation += 0.01
                            self.enemy_multiplicator *= 0.95
                            self.search_bar.update(0, True)
                            if self.enemy_multiplicator < 1:
                                self.state = Constant.LAB_MAX
                            else:
                                self.state = Constant.LAB_W8_4_RSRCH
                        else:
                            self.buy_button.set_state(1)
                    else:
                        self.buy_button.set_state(2)
        elif self.state == Constant.LAB_IN_RSRCH:
            if self.search_bar.update((elapsed_time / self.wait_time) * 100):
                free_scientists += int(self.nb_scientists)
                self.state = Constant.LAB_W8_4_BUY
        return money, scientists, scientists_limit, free_scientists

    def draw(self, screen):
        if self.state == Constant.LAB_W8_4_RSRCH:
            text_surface = self.font.render("Research baddest monsters", True, [200, 200, 255])
            screen.blit(text_surface, [700, Constant.INTRF_BOTTOM_BAR_Y + 10])
            text_surface = self.font.render("You need " + str(int(self.nb_scientists)) + " scientists", True, [200, 200, 255])
            screen.blit(text_surface, [700, Constant.INTRF_BOTTOM_BAR_Y + 30])
            self.search_button.draw(screen)
        elif self.state == Constant.LAB_W8_4_BUY:
            text_surface = self.font.render("This monster look very bad...", True, [200, 200, 255])
            screen.blit(text_surface, [700, Constant.INTRF_BOTTOM_BAR_Y + 10])
            text_surface = self.font.render("Just " + str(int(self.price)) + "$ to try it!", True, [200, 200, 255])
            screen.blit(text_surface, [700, Constant.INTRF_BOTTOM_BAR_Y + 30])
            self.buy_button.draw(screen)
        elif self.state == Constant.LAB_IN_RSRCH:
            text_surface = self.font.render("Researching ...", True, [200, 200, 255])
            screen.blit(text_surface, [700, Constant.INTRF_BOTTOM_BAR_Y + 30])
            self.search_bar.draw(screen)

class Armor_Action:

    def __init__(self, ship):
        self.state = Constant.LAB_W8_4_RSRCH
        self.ship = ship
        self.price = 40
        self.wait_time = 8000
        self.nb_scientists = 2
        self.augmentation = 1.35
        self.armor_multiplicator = 1.5
        self.font = pygame.font.Font(Constant.FONT_PATH, 15)
        self.search_button = User_interface.Button(Constant.FONT_PATH, [250, 25],
                                                   [700, Constant.INTRF_BOTTOM_BAR_Y + 60], 2,
                                                   Constant.UNBUYABLE_BUTTON, {
                                                       "TXT": "Launch Research",
                                                       "SIZE": 15,
                                                       "COLOR": [[200, 200, 200], [250, 250, 250], [250, 250, 250]]})
        self.buy_button = User_interface.Button(Constant.FONT_PATH, [250, 25],
                                                [700, Constant.INTRF_BOTTOM_BAR_Y + 60], 2,
                                                Constant.UNBUYABLE_BUTTON, {
                                                    "TXT": "Buy it !",
                                                    "SIZE": 15,
                                                    "COLOR": [[200, 200, 200], [250, 250, 250], [250, 250, 250]]})
        self.search_bar = User_interface.Progress_Bar([700, Constant.INTRF_BOTTOM_BAR_Y + 60], [200, 15], 2, [[255, 255, 255], [200, 200, 200], [0, 0, 255]], 100)

    def update(self, money, scientists, scientists_limit, free_scientists, elapsed_time, state):
        if not state and self.state == Constant.LAB_W8_4_RSRCH:
            if int(self.nb_scientists) > free_scientists:
                self.search_button.set_color(Constant.UNBUYABLE_BUTTON)
            else:
                self.search_button.set_color(Constant.BUYABLE_BUTTON)
                if self.search_button.get_state() != 0 or not pygame.mouse.get_pressed()[0]:
                    if self.search_button.is_overflew():
                        if pygame.mouse.get_pressed()[0]:
                            free_scientists -= int(self.nb_scientists)
                            self.search_button.set_state(0)
                            self.state = Constant.LAB_IN_RSRCH
                        else:
                            self.search_button.set_state(1)
                    else:
                        self.search_button.set_state(2)
        elif not state and self.state == Constant.LAB_W8_4_BUY:
            if self.price > money:
                self.buy_button.set_color(Constant.UNBUYABLE_BUTTON)
            else:
                self.buy_button.set_color(Constant.BUYABLE_BUTTON)
                if self.buy_button.get_state() != 0 or not pygame.mouse.get_pressed()[0]:
                    if self.buy_button.is_overflew():
                        if pygame.mouse.get_pressed()[0]:
                            self.ship.increase_life(self.armor_multiplicator)
                            self.buy_button.set_state(0)
                            money -= self.price
                            self.price *= self.augmentation
                            self.wait_time *= self.augmentation
                            self.nb_scientists *= self.augmentation
                            self.augmentation += 0.01
                            self.armor_multiplicator *= 0.99
                            self.search_bar.update(0, True)
                            if self.armor_multiplicator < 1:
                                self.state = Constant.LAB_MAX
                            else:
                                self.state = Constant.LAB_W8_4_RSRCH
                        else:
                            self.buy_button.set_state(1)
                    else:
                        self.buy_button.set_state(2)
        elif self.state == Constant.LAB_IN_RSRCH:
            if self.search_bar.update((elapsed_time / self.wait_time) * 100):
                free_scientists += int(self.nb_scientists)
                self.state = Constant.LAB_W8_4_BUY
        return money, scientists, scientists_limit, free_scientists

    def draw(self, screen):
        if self.state == Constant.LAB_W8_4_RSRCH:
            text_surface = self.font.render("Research a new technology", True, [200, 200, 255])
            screen.blit(text_surface, [700, Constant.INTRF_BOTTOM_BAR_Y + 10])
            text_surface = self.font.render("You need " + str(int(self.nb_scientists)) + " scientists", True, [200, 200, 255])
            screen.blit(text_surface, [700, Constant.INTRF_BOTTOM_BAR_Y + 30])
            self.search_button.draw(screen)
        elif self.state == Constant.LAB_W8_4_BUY:
            text_surface = self.font.render("What d'you think of our new", True, [200, 200, 255])
            screen.blit(text_surface, [700, Constant.INTRF_BOTTOM_BAR_Y + 10])
            text_surface = self.font.render("armor ? Only " + str(int(self.price)) + "$ !", True, [200, 200, 255])
            screen.blit(text_surface, [700, Constant.INTRF_BOTTOM_BAR_Y + 30])
            self.buy_button.draw(screen)
        elif self.state == Constant.LAB_IN_RSRCH:
            text_surface = self.font.render("Researching ...", True, [200, 200, 255])
            screen.blit(text_surface, [700, Constant.INTRF_BOTTOM_BAR_Y + 30])
            self.search_bar.draw(screen)

class Weapon_Action:

    def __init__(self, ship):
        self.state = Constant.LAB_W8_4_RSRCH
        self.ship = ship
        self.price = 50
        self.wait_time = 10000
        self.nb_scientists = 3
        self.augmentation = 1.35
        self.speed_dam_multiplicator = 1.8
        self.font = pygame.font.Font(Constant.FONT_PATH, 15)
        self.search_button = User_interface.Button(Constant.FONT_PATH, [250, 25],
                                                   [700, Constant.INTRF_BOTTOM_BAR_Y + 60], 2,
                                                   Constant.UNBUYABLE_BUTTON, {
                                                       "TXT": "Launch Research",
                                                       "SIZE": 15,
                                                       "COLOR": [[200, 200, 200], [250, 250, 250], [250, 250, 250]]})
        self.buy_button = User_interface.Button(Constant.FONT_PATH, [250, 25],
                                                [700, Constant.INTRF_BOTTOM_BAR_Y + 60], 2,
                                                Constant.UNBUYABLE_BUTTON, {
                                                    "TXT": "Buy it !",
                                                    "SIZE": 15,
                                                    "COLOR": [[200, 200, 200], [250, 250, 250], [250, 250, 250]]})
        self.search_bar = User_interface.Progress_Bar([700, Constant.INTRF_BOTTOM_BAR_Y + 60], [200, 15], 2, [[255, 255, 255], [200, 200, 200], [0, 0, 255]], 100)

    def update(self, money, scientists, scientists_limit, free_scientists, elapsed_time, state):
        if not state and self.state == Constant.LAB_W8_4_RSRCH:
            if int(self.nb_scientists) > free_scientists:
                self.search_button.set_color(Constant.UNBUYABLE_BUTTON)
            else:
                self.search_button.set_color(Constant.BUYABLE_BUTTON)
                if self.search_button.get_state() != 0 or not pygame.mouse.get_pressed()[0]:
                    if self.search_button.is_overflew():
                        if pygame.mouse.get_pressed()[0]:
                            free_scientists -= int(self.nb_scientists)
                            self.search_button.set_state(0)
                            self.state = Constant.LAB_IN_RSRCH
                        else:
                            self.search_button.set_state(1)
                    else:
                        self.search_button.set_state(2)
        elif not state and self.state == Constant.LAB_W8_4_BUY:
            if self.price > money:
                self.buy_button.set_color(Constant.UNBUYABLE_BUTTON)
            else:
                self.buy_button.set_color(Constant.BUYABLE_BUTTON)
                if self.buy_button.get_state() != 0 or not pygame.mouse.get_pressed()[0]:
                    if self.buy_button.is_overflew():
                        if pygame.mouse.get_pressed()[0]:
                            self.ship.increase_damage(self.speed_dam_multiplicator)
                            self.ship.increase_speed(self.speed_dam_multiplicator)
                            self.buy_button.set_state(0)
                            money -= self.price
                            self.price *= self.augmentation
                            self.wait_time *= self.augmentation
                            self.nb_scientists *= self.augmentation
                            self.augmentation += 0.01
                            self.speed_dam_multiplicator *= 0.95
                            self.search_bar.update(0, True)
                            if self.speed_dam_multiplicator < 1:
                                self.state = Constant.LAB_MAX
                            else:
                                self.state = Constant.LAB_W8_4_RSRCH
                        else:
                            self.buy_button.set_state(1)
                    else:
                        self.buy_button.set_state(2)
        elif self.state == Constant.LAB_IN_RSRCH:
            if self.search_bar.update((elapsed_time / self.wait_time) * 100):
                free_scientists += int(self.nb_scientists)
                self.state = Constant.LAB_W8_4_BUY
        return money, scientists, scientists_limit, free_scientists

    def draw(self, screen):
        if self.state == Constant.LAB_W8_4_RSRCH:
            text_surface = self.font.render("Research a new technology", True, [200, 200, 255])
            screen.blit(text_surface, [700, Constant.INTRF_BOTTOM_BAR_Y + 10])
            text_surface = self.font.render("You need " + str(int(self.nb_scientists)) + " scientists", True, [200, 200, 255])
            screen.blit(text_surface, [700, Constant.INTRF_BOTTOM_BAR_Y + 30])
            self.search_button.draw(screen)
        elif self.state == Constant.LAB_W8_4_BUY:
            text_surface = self.font.render("We just discovered a new", True, [200, 200, 255])
            screen.blit(text_surface, [700, Constant.INTRF_BOTTOM_BAR_Y + 10])
            text_surface = self.font.render("weapon of doom for " + str(int(self.price)) + "$ !", True, [200, 200, 255])
            screen.blit(text_surface, [700, Constant.INTRF_BOTTOM_BAR_Y + 30])
            self.buy_button.draw(screen)
        elif self.state == Constant.LAB_IN_RSRCH:
            text_surface = self.font.render("Researching ...", True, [200, 200, 255])
            screen.blit(text_surface, [700, Constant.INTRF_BOTTOM_BAR_Y + 30])
            self.search_bar.draw(screen)

class Hangar_Action:

    def __init__(self, ship):
        self.ship = ship
        self.cost = 0
        self.repaired = 0
        self.price_per_unit = 2.5
        self.font = pygame.font.Font(Constant.FONT_PATH, 15)
        self.repair_button = User_interface.Button(Constant.FONT_PATH, [250, 25],
                                                         [700, Constant.INTRF_BOTTOM_BAR_Y + 60], 2,
                                                         Constant.UNBUYABLE_BUTTON, {
                                                             "TXT": "Repair your ship",
                                                             "SIZE": 15,
                                                             "COLOR": [[200, 200, 200], [250, 250, 250], [250, 250, 250]]})

    def update(self, money, scientists, scientists_limit, free_scientists, elapsed_time, state):
        if state != 0:
            return money, scientists, scientists_limit, free_scientists
        self.cost = self.price_per_unit * self.ship.get_missing_life()
        self.repaired = self.ship.get_missing_life()
        if self.cost > money:
            self.repaired = int(money / self.price_per_unit)
            self.cost = self.price_per_unit * self.repaired
        if money == 0 or self.ship.get_missing_life() == 0:
            self.repair_button.set_color(Constant.UNBUYABLE_BUTTON)
        else:
            self.repair_button.set_color(Constant.BUYABLE_BUTTON)
            if self.repair_button.get_state() != 0 or not pygame.mouse.get_pressed()[0]:
                if self.repair_button.is_overflew():
                    if pygame.mouse.get_pressed()[0]:
                        self.repair_button.set_state(0)
                        money -= self.cost
                        self.ship.repair(self.repaired)
                    else:
                        self.repair_button.set_state(1)
                else:
                    self.repair_button.set_state(2)
        return money, scientists, scientists_limit, free_scientists

    def draw(self, screen):
        self.repair_button.draw(screen)
        text_surface = self.font.render(str(int(self.cost)) + "$ for " + str(int(self.repaired / self.ship.get_max_life() * 100)) + " percents:", True, [200, 200, 255])
        screen.blit(text_surface, [700, Constant.INTRF_BOTTOM_BAR_Y + 40])

class Recruitement_Action:

    def __init__(self):
        self.price = 20
        self.price_size_increase = 100
        self.augmentation_size_increase = 1.2
        self.augmentation = 1.1
        self.font = pygame.font.Font(Constant.FONT_PATH, 30)
        self.recruitement_button = User_interface.Button(Constant.FONT_PATH, [250, 25],
                                                         [700, Constant.INTRF_BOTTOM_BAR_Y + 60], 2,
                                                         Constant.UNBUYABLE_BUTTON, {
                                                             "TXT": "Recruit a scientist",
                                                             "SIZE": 15,
                                                             "COLOR": [[200, 200, 200], [250, 250, 250], [250, 250, 250]]})
        self.increase_button = User_interface.Button(Constant.FONT_PATH, [250, 25],
                                                     [700, Constant.INTRF_BOTTOM_BAR_Y + 130], 2,
                                                     Constant.UNBUYABLE_BUTTON, {
                                                         "TXT": "Increase scientist stack",
                                                         "SIZE": 15,
                                                         "COLOR": [[200, 200, 200], [250, 250, 250], [250, 250, 250]]})

    def update(self, money, scientists, scientists_limit, free_scientists, elapsed_time, state):
        if state != 0:
            return money, scientists, scientists_limit, free_scientists
        if self.price > money or scientists >= scientists_limit:
            self.recruitement_button.set_color(Constant.UNBUYABLE_BUTTON)
        else:
            self.recruitement_button.set_color(Constant.BUYABLE_BUTTON)
            if self.recruitement_button.get_state() != 0 or not pygame.mouse.get_pressed()[0]:
                if self.recruitement_button.is_overflew():
                    if pygame.mouse.get_pressed()[0]:
                        scientists += 1
                        free_scientists += 1
                        self.recruitement_button.set_state(0)
                        money -= self.price
                        self.price *= self.augmentation
                        self.augmentation += 0.02
                    else:
                        self.recruitement_button.set_state(1)
                else:
                    self.recruitement_button.set_state(2)
        if self.price_size_increase > money:
            self.increase_button.set_color(Constant.UNBUYABLE_BUTTON)
        else:
            self.increase_button.set_color(Constant.BUYABLE_BUTTON)
            if self.increase_button.get_state() != 0 or not pygame.mouse.get_pressed()[0]:
                if self.increase_button.is_overflew():
                    if pygame.mouse.get_pressed()[0]:
                        scientists_limit += 5
                        self.increase_button.set_state(0)
                        money -= self.price_size_increase
                        self.price_size_increase *= self.augmentation_size_increase
                        self.augmentation_size_increase += 0.15
                    else:
                        self.increase_button.set_state(1)
                else:
                    self.increase_button.set_state(2)
        return money, scientists, scientists_limit, free_scientists

    def draw(self, screen):
        self.recruitement_button.draw(screen)
        text_surface = self.font.render(str(int(self.price)) + "$ :", True, [200, 200, 255])
        screen.blit(text_surface, [700, Constant.INTRF_BOTTOM_BAR_Y + 20])
        self.increase_button.draw(screen)
        text_surface = self.font.render(str(int(self.price_size_increase)) + "$ :", True, [200, 200, 255])
        screen.blit(text_surface, [700, Constant.INTRF_BOTTOM_BAR_Y + 100])

class Building:

    def __init__(self, name, desc, image, size, pos, price, margin, action):
        self.scientist_skin = pygame.image.load(Constant.ASSETS_DIR + "scientist.png")
        self.scientist_skin = pygame.transform.scale(self.scientist_skin, [self.scientist_skin.get_size()[0] * 2, self.scientist_skin.get_size()[1] * 2])
        self.scientists = 0
        self.action = action
        self.name = name
        self.desc = desc
        self.image = pygame.transform.scale(pygame.image.load(Constant.ASSETS_DIR + image), size)
        self.pos = pos
        self.price = price
        self.font = pygame.font.Font(Constant.FONT_PATH, 30)
        self.small_font = pygame.font.Font(Constant.FONT_PATH, 15)
        if self.price > 0:
            self.locker = pygame.transform.scale(pygame.image.load(Constant.ASSETS_DIR + "lock.png"), [size[0] // 3, size[1] // 3])
            self.locker_pos = [self.pos[0] + size[0] // 3, self.pos[1] + 1 * size[1] // 3]
            self.unlockable_color_button = Constant.BUYABLE_BUTTON
            self.nonunlockable_color_button = Constant.UNBUYABLE_BUTTON
            self.unlock_button = User_interface.Button(Constant.FONT_PATH, [300, 35],
                                                       [20, Constant.INTRF_BOTTOM_BAR_Y + 60], 2,
                                                       self.nonunlockable_color_button, {
                                                           "TXT": "UNLOCK (" + str(self.price) + "$)",
                                                           "SIZE": 30,
                                                           "COLOR": [[200, 200, 200], [250, 250, 250], [250, 250, 250]]})

        self.button = User_interface.Button("", [size[0] + margin * 2, size[1] + margin * 2],
                                            [pos[0] - margin, pos[1] - margin], 2,
                                            [[200, 200, 200, 100], [150, 150, 150, 150], [0, 0, 0, 0]])

    def update(self, money, scientists, scientists_limit, free_scientists, elapsed_time):
        if self.price <= 0:
            money, scientists, scientists_limit, free_scientists = self.action.update(money, scientists, scientists_limit, free_scientists, elapsed_time, self.button.get_state())
        if self.button.get_state() == 0:
            if self.price > 0:
                if self.price > money:
                    self.unlock_button.set_color(self.nonunlockable_color_button)
                else:
                    self.unlock_button.set_color(self.unlockable_color_button)
                    if self.unlock_button.is_overflew():
                        if pygame.mouse.get_pressed()[0]:
                            money -= self.price
                            self.price = -1
                        else:
                            self.unlock_button.set_state(1)
                    else:
                        self.unlock_button.set_state(2)

        if (pygame.mouse.get_pressed()[0] and pygame.mouse.get_pos()[1] < Constant.INTRF_BOTTOM_BAR_Y) or self.button.get_state() != 0:
            if self.button.is_overflew():
                if pygame.mouse.get_pressed()[0]:
                    self.button.set_state(0)
                else:
                    self.button.set_state(1)
            else:
                self.button.set_state(2)
        return money, scientists, scientists_limit, free_scientists

    def attribute_scientist(self, nb, absolute = False):
        self.scientists += nb
        if absolute:
            self.scientists = nb

    def draw(self, screen):
        if self.price > 0:
            screen.blit(self.locker, self.locker_pos)
        else:
            screen.blit(self.image, self.pos)
            pos = [self.pos[0], self.pos[1] + self.image.get_size()[1] - 40]
            for i in range(self.scientists):
                screen.blit(self.scientist_skin, pos)
                pos[0] += self.scientist_skin.get_size()[0] * 1.1
                if pos[0] > self.pos[0] + self.image.get_size()[0]:
                    pos[0] = self.pos[0]
                    pos[1] += self.scientist_skin.get_size()[1] / 2
        self.button.draw(screen)
        if self.button.get_state() == 0:
            text_surface = self.font.render(self.name, True, [200, 200, 230])
            screen.blit(text_surface, [20, Constant.INTRF_BOTTOM_BAR_Y + 20])
            text_surface = self.small_font.render(self.desc, True, [255, 255, 255])
            screen.blit(text_surface, [20, Constant.INTRF_BOTTOM_BAR_Y + 100])
            if self.price > 0:
                self.unlock_button.draw(screen)
            else:
                self.action.draw(screen)
