##
## Event Manager
##

import pygame
import sys

def manager(ship, scop):
    for event in pygame.event.get():
        if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.dict["key"] == pygame.K_ESCAPE):
            sys.exit()
        if scop == "MANAGER":
            return
        if event.type == pygame.KEYUP and event.dict["key"] == pygame.K_LEFT:
            ship.move_left(False)
        if event.type == pygame.KEYUP and event.dict["key"] == pygame.K_RIGHT:
            ship.move_right(False)
        if event.type == pygame.KEYUP and event.dict["key"] == pygame.K_SPACE:
            ship.shoot(False)
        if event.type == pygame.KEYDOWN and event.dict["key"] == pygame.K_SPACE:
            ship.shoot(True)
        if event.type == pygame.KEYDOWN and event.dict["key"] == pygame.K_LEFT:
            ship.move_left(True)
        if event.type == pygame.KEYDOWN and event.dict["key"] == pygame.K_RIGHT:
            ship.move_right(True)
