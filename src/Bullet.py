##
## Bullet
##

import pygame

from . import Constant

class MyBullet:

    def __init__(self, pos, damage):
        self.pos = pos
        self.speed = [0, -5]
        self.damage = damage
        self.skin = pygame.image.load(Constant.ASSETS_DIR + "mine-bullet.png")

    def get_dimention(self):
        return pygame.Rect(self.pos, self.skin.get_size())

    def update(self):
        self.pos[0] += self.speed[0]
        self.pos[1] += self.speed[1]
        if self.pos[1] < 0:
            return False
        return True

    def draw(self, screen):
        screen.blit(self.skin, self.pos)
