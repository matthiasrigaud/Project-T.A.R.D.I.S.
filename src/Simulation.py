##
## Interface for the manager part
##

import pygame

from . import Constant, User_interface, Enemy

from random import randint

class Simulation:

    def __init__(self, enemy_skeleton, ship):
        self.enemy_skeleton = enemy_skeleton
        self.ship = ship
        self.money = 0
        self.back_img = pygame.image.load(Constant.ASSETS_DIR + "back-space.png")
        self.money_icon = pygame.transform.scale(pygame.image.load(Constant.ASSETS_DIR + "thune.png"), [40, 40])
        self.font = pygame.font.Font(Constant.FONT_PATH, 40)
        self.ship_life = User_interface.Progress_Bar([Constant.INTRF_SIDE_BAR_X + 80, 150], [200, 30], 2, [[255, 255, 255], [255, 0, 0], [0, 255, 0]], 100)
        self.time_bar = User_interface.Progress_Bar([Constant.INTRF_SIDE_BAR_X + 10, 270], [280, 30], 2, [[255, 255, 255], [220, 220, 220], [0, 0, 255]], 100)
        self.time_remaining = 0
        self.begin_time = 0
        self.enemies = []

    def update(self, elapsed_time, money, time_remaining):
        self.money = money
        if time_remaining > 0:
            self.ship.hard_movement_reset()
            self.ship.shoot(False)
            self.time_remaining = time_remaining
            self.begin_time = time_remaining
            del self.enemies[:]
            return "SIMULATION", self.money
        self.time_remaining -= elapsed_time
        if self.time_remaining <= 0 or self.ship.get_life_percent() == 0:
            return "MANAGER", self.money
        if randint(20, 45) == 42:
            self.enemies.append(Enemy.Enemy(self.enemy_skeleton))
        self.ship_life.update(self.ship.get_life_percent(), True)
        self.time_bar.update(100 - self.time_remaining / self.begin_time * 100, True)
        for i in iter(self.enemies):
            if not i.update() or self.ship.collision(i):
                self.enemies.remove(i)
        self.money = self.ship.update(elapsed_time, self.enemies, self.money)
        return "SIMULATION", self.money

    def draw(self, screen):
        for x in range(0, Constant.WIN_WIDTH, self.back_img.get_size()[0]):
            for y in range(0, Constant.WIN_HEIGHT, self.back_img.get_size()[1]):
                screen.blit(self.back_img, [x, y])
        pygame.draw.rect(screen, Constant.INTRF_SIDE_BAR_COLOR, pygame.Rect([Constant.INTRF_SIDE_BAR_X, 0],
                                                                            [Constant.INTRF_SIDE_BAR_WIDTH, Constant.WIN_HEIGHT]))
        screen.blit(self.money_icon, [Constant.INTRF_SIDE_BAR_X + 20, 40])
        text_surface = self.font.render(str(int(self.money)), True, [200, 200, 230])
        screen.blit(text_surface, [Constant.INTRF_SIDE_BAR_X + 85, 45])
        self.ship_life.draw(screen)
        self.ship.draw(screen, [Constant.INTRF_SIDE_BAR_X + 10, 130])
        text_surface = self.font.render("Time", True, [200, 200, 230])
        screen.blit(text_surface, [Constant.INTRF_SIDE_BAR_X + 90, 230])
        self.time_bar.draw(screen)
        self.ship.draw(screen)
        for i in iter(self.enemies):
            i.draw(screen)
