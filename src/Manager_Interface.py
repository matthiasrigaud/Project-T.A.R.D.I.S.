##
## Interface for the manager part
##

import pygame

from . import Constant, User_interface, Building

class Manager_Interface:

    def __init__(self, scientists, scientists_limit, ship, enemy_skeleton):
        self.enemy_skeleton = enemy_skeleton
        self.ship = ship
        self.money = 0
        self.scientists_limit = scientists_limit
        self.scientists = scientists
        self.free_scientists = scientists
        self.font = pygame.font.Font(Constant.FONT_PATH, 40)
        self.money_icon = pygame.transform.scale(pygame.image.load(Constant.ASSETS_DIR + "thune.png"), [40, 40])
        self.scientist_icon = pygame.transform.scale(pygame.image.load(Constant.ASSETS_DIR + "scientist-head.png"), [50, 50])
        self.ground = pygame.image.load(Constant.ASSETS_DIR + "ground_texture.png")
        self.buildings = {}
        self.buildings["WEAPON"] = Building.Building("Weapon Laboratory", Constant.WEAPON_DESC, "weapon-laboratory.png",
                                                     [200, 200], [95, 50], 150, 4, Building.Weapon_Action(ship))
        self.buildings["ARMOR"] = Building.Building("Armor Laboratory", Constant.ARMOR_DESC, "armor-laboratory.png",
                                                    [200, 200], [390, 50], 120, 4, Building.Armor_Action(ship))
        self.buildings["ENEMY"] = Building.Building("Enemy Laboratory", Constant.ENEMY_DESC, "enemy-laboratory.png",
                                                    [200, 200], [685, 50], 200, 4, Building.Enemy_Action(self.enemy_skeleton))
        self.buildings["HANGAR"] = Building.Building("Hangar", Constant.HANGAR_DESC, "hangar-building.png",
                                                     [200, 200], [243, 200], -1, 4, Building.Hangar_Action(ship))
        self.buildings["RECRUITEMENT"] = Building.Building("Recruitement", Constant.RECRUITEMENT_DESC, "recruitement-building.png",
                                                           [200, 200], [538, 200], -1, 4, Building.Recruitement_Action())
        self.ship_life = User_interface.Progress_Bar([Constant.INTRF_SIDE_BAR_X + 80, 250], [200, 30], 2, [[255, 255, 255], [255, 0, 0], [0, 255, 0]], 100)
        self.simulation_button = User_interface.Button(Constant.FONT_PATH, [Constant.INTRF_SIDE_BAR_WIDTH, 200],
                                                       [Constant.INTRF_SIDE_BAR_X, Constant.WIN_HEIGHT - 200], 2,
                                                       [[], [255, 100, 100], [230, 100, 100]], {
                                                           "TXT": "Launch Simulation !",
                                                           "SIZE": 15,
                                                           "COLOR": [[], [250, 250, 250], [200, 200, 200]]})

    def update(self, elapsed_time, money):
        old_free_scientists = self.free_scientists
        self.money = money
        for i in iter(self.buildings):
            self.money, self.scientists, self.scientists_limit, self.free_scientists = self.buildings[i].update(self.money, self.scientists, self.scientists_limit, self.free_scientists, elapsed_time)
            if old_free_scientists != self.free_scientists:
                self.buildings[i].attribute_scientist(old_free_scientists - self.free_scientists)
                old_free_scientists = self.free_scientists
                self.buildings["RECRUITEMENT"].attribute_scientist(self.free_scientists, True)
        self.ship_life.update(self.ship.get_life_percent(), True)
        if self.simulation_button.is_overflew():
            if pygame.mouse.get_pressed()[0] and self.ship.get_life_percent() > 0:
                return "SIMULATION", self.money
            else:
                self.simulation_button.set_state(1)
        else:
            self.simulation_button.set_state(2)
        return "MANAGER", self.money

    def draw(self, screen):
        for x in range(0, Constant.WIN_WIDTH, self.ground.get_size()[0]):
            for y in range(0, Constant.WIN_HEIGHT, self.ground.get_size()[1]):
                screen.blit(self.ground, [x, y])
        pygame.draw.rect(screen, Constant.INTRF_SIDE_BAR_COLOR, pygame.Rect([Constant.INTRF_SIDE_BAR_X, 0],
                                                                            [Constant.INTRF_SIDE_BAR_WIDTH, Constant.WIN_HEIGHT]))
        pygame.draw.rect(screen, Constant.INTRF_BOTTOM_BAR_COLOR, pygame.Rect([0, Constant.INTRF_BOTTOM_BAR_Y],
                                                                              [Constant.INTRF_BOTTOM_BAR_WIDTH, Constant.INTRF_BOTTOM_BAR_HEIGHT]))
        screen.blit(self.money_icon, [Constant.INTRF_SIDE_BAR_X + 20, 40])
        text_surface = self.font.render(str(int(self.money)), True, [200, 200, 230])
        screen.blit(text_surface, [Constant.INTRF_SIDE_BAR_X + 85, 45])
        screen.blit(self.scientist_icon, [Constant.INTRF_SIDE_BAR_X + 20, 130])
        text_surface = self.font.render(str(self.scientists) + " / " + str(self.scientists_limit), True, [200, 200, 230])
        screen.blit(text_surface, [Constant.INTRF_SIDE_BAR_X + 85, 138])
        for i in iter(self.buildings):
            self.buildings[i].draw(screen)
        self.ship_life.draw(screen)
        self.ship.draw(screen, [Constant.INTRF_SIDE_BAR_X + 10, 230])
        text_surface = self.font.render("Armor : " + str(int(self.ship.get_max_life())), True, [200, 200, 230])
        screen.blit(text_surface, [Constant.INTRF_SIDE_BAR_X + 10, 300])
        text_surface = self.font.render("Damage : " + str(int(self.ship.get_damage())), True, [200, 200, 230])
        screen.blit(text_surface, [Constant.INTRF_SIDE_BAR_X + 10, 350])
        text_surface = self.font.render("Speed : " + str(int(self.ship.get_speed())), True, [200, 200, 230])
        screen.blit(text_surface, [Constant.INTRF_SIDE_BAR_X + 10, 400])
        self.simulation_button.draw(screen)
