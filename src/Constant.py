#### CONSTANT GLOBAL VALUES ####

WIN_WIDTH       = 1280
WIN_HEIGHT      = 720
WIN_TITLE       = "Project T.A.R.D.I.S."

FRAME_TIME      = 35

## interface default

NB_SCIENTISTS           = 0
SCIENTISTS_LIMIT        = 5

INTRF_SIDE_BAR_COLOR    = [120, 120, 120]
INTRF_SIDE_BAR_WIDTH    = 300
INTRF_SIDE_BAR_X        = WIN_WIDTH - INTRF_SIDE_BAR_WIDTH

INTRF_BOTTOM_BAR_COLOR  = [130, 130, 130]
INTRF_BOTTOM_BAR_HEIGHT = 200
INTRF_BOTTOM_BAR_WIDTH  = WIN_WIDTH - INTRF_SIDE_BAR_WIDTH
INTRF_BOTTOM_BAR_Y      = WIN_HEIGHT - INTRF_BOTTOM_BAR_HEIGHT

## assets

ASSETS_DIR      = "./assets/"
FONT_DIR        = ASSETS_DIR + "font/"
FONT_PATH       = FONT_DIR + "propaganda.ttf"

## Descriptions

WEAPON_DESC       = "The weapon laboratory is specialised in research concerning gun speed and power."
ARMOR_DESC        = "Increase the armor of your ship. Like a rock !"
ENEMY_DESC        = "\"With great power comes great responsibility\" ... Better monsters mean more money !"
HANGAR_DESC       = "Kind of Batcave : here you can repair you ship and get some cookies."
RECRUITEMENT_DESC = "Rise an army of mad scientists !"

## Buttons

BUYABLE_BUTTON          = [[0, 0, 0], [150, 250, 150], [150, 200, 150]]
UNBUYABLE_BUTTON        = [[100, 100, 100], [100, 100, 100], [100, 100, 100]]

## Laboratory states

LAB_IN_RSRCH    = 0
LAB_W8_4_RSRCH  = 1
LAB_W8_4_BUY    = 2
LAB_MAX         = 3
