##
## define bases stats for enemies
##

class Enemy_skeleton:

    def __init__(self):
        self.armor = 8
        self.speed = 0.7
        self.damage = 5
        self.gain = 5

    def get_armor(self):
        return self.armor

    def get_speed(self):
        return self.speed

    def get_damage(self):
        return self.damage

    def get_gain(self):
        return self.gain

    def increase_stats(self, multiplicator):
        self.armor *= 1.5 * multiplicator
        self.speed /= 1.2 * multiplicator
        self.damage *= 1.5 * multiplicator
        self.gain *= 1.1 * multiplicator
